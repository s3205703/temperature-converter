package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Converter {

    public double getBookPrice(String temperature) {
        return Integer.parseInt(temperature) * 1.8 + 32;
    }
}