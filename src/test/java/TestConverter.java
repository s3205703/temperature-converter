import nl.utwente.di.bookQuote.Converter;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class TestConverter {
    @Test
    public void TestQuoter() throws Exception {
        Converter converter = new Converter();
        double price = converter.getBookPrice("1");
        Assertions.assertEquals(10.0, price, 0.0);
    }
}